README
======

Use this Vagrant configuration to setup your CoreOS and Docker based development environment.
- - - 

Prerequisite
------------

- [VirtualBox](https://www.virtualbox.org/)
- [Vagrant](https://www.vagrantup.com/)

How to use?
-----------
- Clone this repository  
  `git clone https://munjalpatel@bitbucket.org/munjalpatel/environments.git`

- Navigate to the diectory

- Copy `config.rb.template` to `config.rb` and update the required configurations

- Start Vagrant  
  `vagrant up`

- SSH into your development environment  
  `vagrant ssh`

CoreOS Services
---------------

This Vagrant configuration can automatically start all your services upon `vagrant up`.  
Please define all your services in `*.service` file in your `apps` directory that you configured in `config.rb`.

####Example service definition [<apps_dir>/myapp/myapp.service]
    [Unit]
    Description=MyApp
    After=docker.service
    Requires=docker.service

    [Service]
    TimeoutStartSec=0
    ExecStartPre=-/usr/bin/docker kill myapp
    ExecStartPre=-/usr/bin/docker rm myapp
    ExecStartPre=/usr/bin/docker build -t munjalpatel/myapp /home/core/apps/myapp/.
    ExecStart=/usr/bin/docker run --name myapp -d -p 8080:8080 munjalpatel/myapp

    [Install]
    WantedBy=myapp.target
    
How to contribute?
------------------

Your contributions are always welcome :)  
Please fork this repository and make a pull request when ready!

Core contributors
-----------------

- Munjal Patel <munjal@munpat.com>